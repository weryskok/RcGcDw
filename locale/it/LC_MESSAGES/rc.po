# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the RcGcDw package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: RcGcDw\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2020-11-30 11:58+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: Automatically generated\n"
"Language-Team: none\n"
"Language: it\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: src/rc.py:321
#, python-brace-format
msgid "{wiki} seems to be down or unreachable."
msgstr ""

#: src/rc.py:322 src/rc.py:334
msgid "Connection status"
msgstr ""

#: src/rc.py:332
#, python-brace-format
msgid "Connection to {wiki} seems to be stable now."
msgstr ""

#: src/rc.py:403
msgid "~~hidden~~"
msgstr ""

#: src/rc.py:407
msgid "hidden"
msgstr ""
