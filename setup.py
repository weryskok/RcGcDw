from setuptools import setup, find_packages

setup(
	name='RcGcDw',
	version='1.15.0.3',
	url='https://gitlab.com/piotrex43/RcGcDw/',
	license='GNU GPLv3',
	author='Frisk',
	author_email='piotrex43@protonmail.ch',
	description='A set od scripts to fetch recent changes from MediaWiki wiki to a Discord channel using a webhook',
	keywords=['MediaWiki', 'recent changes', 'Discord', 'webhook'],
    packages=['src'],
	package_dir={
		"src": "src",
		"src.api": "src/api",
		"src.discord": "src/discord",
		"src.fileio": "src/fileio",
		"src.migrations": "src/migrations"
	},
	install_requires=["beautifulsoup4 >= 4.6.0", "requests >= 2.18.4", "lxml >= 4.2.1"],
	python_requires=">=3.7",
	scripts=["src/rcgcdw.py"]
)
